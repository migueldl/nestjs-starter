const myName = 'Nicolas';
const myAge = 23;
const suma = (a: number, b: number) => {
  return a + b;
};

suma('12', '23');

class Persona {

  constructor(private age: number, private name: string) {}

  getSummary() {
    return `My name is ${this.name}`;
    return `My age is ${this.age}`;
  }
}

const mike = new Persona(23, 'Miguel de Leon');
mike.getSummary();
